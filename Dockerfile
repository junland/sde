FROM archlinux/base:latest

ARG dARCH=x86_64

ENV BARCH=$dARCH

ENV CI_ARCHIVE="1"

WORKDIR /root

RUN pacman -Syu --noconfirm

RUN pacman -S --noconfirm base-devel xorriso mtools git pigz python python2 pixz wget curl zlib xz libarchive libtool perl bc autoconf-archive automake autoconf

RUN git clone https://github.com/project-gemstone/pkgutils.git && cd pkgutils && make -f Makefile.dynamic && make install

RUN git clone --single-branch --branch backup https://gitlab.com/junland/sde.git 

RUN sed -i "s/EUID == 0/EUID == 3/g" /usr/bin/makepkg

WORKDIR /root/sde

RUN make install

RUN BARCH=x86_64 wok bootstrap

CMD ["/usr/bin/wok", "build-core-chroot"]